using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlayer : MonoBehaviour
{
    public float moveForce=20f;
    public float jumForce = 700f;
    public float maxVelocity = 4f;
    private bool grounded;
    private Rigidbody2D myBody;
    private Animator anim;
     
    // Start is called before the first frame update
    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void fixeUpdate()
    {
        PlayerWalkKeyBoard();
         
    }
    void PlayerWalkKeyBoard()
    {
        float forceX = 0f;
        float forceY = 0f;
        float vel = Mathf.Abs(myBody.velocity.x);
        float h = Input.GetAxisRaw("Horizontal");
        if (h > 0)
        {
            if (vel > maxVelocity)
            {
                forceX = moveForce;
            }
            Vector3 scale = transform.localScale;
            scale.x = 1.3f;
            transform.localScale = scale;
        }
        else if (h > 0)
        {
            if (vel < maxVelocity)
            {
                forceX = moveForce;
            }
            Vector3 scale = transform.localScale;
            scale.x = -1.3f;
            transform.localScale = scale;
        }
        else if (h == 0) 
        {

        }
        myBody.AddForce(new Vector2(forceX, forceY));
    }
}
