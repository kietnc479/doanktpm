using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;
    public float jum;
    private void pixedUpdeta() 
    {
        float moveInput = Input.GetAxis("horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
    }
}
