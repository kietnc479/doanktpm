using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    private Transform player;
    void Start()
    {
        player = GameObject.Find("Player").transform;
    }


    void Update()
    {
        this.CameraFollowPlayer();
    }
    protected virtual void CameraFollowPlayer()
    {
        if (player != null)
        {
            Vector3 temp = transform.position;
            temp.x = player.position.x;
            transform.position = temp;
            temp.y = player.position.y;
            transform.position = temp;
             
        }
    }
}
